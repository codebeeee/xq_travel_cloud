export default {

	// 放入缓存中
	set(key, value) {
		var key = 'pug_app_' + key;
		try {
			// 全局里放globalData
			getApp().globalData[key] = value;
			// 缓存里也放入
			uni.setStorageSync(key, value)
		} catch (e) {
			console.log("cache error");
		}
	},

	// 从缓存中获取
	get(key) {
		var key = 'pug_app_' + key;
		try {
			// 全局里放
			return uni.getStorageSync(key) || getApp().globalData[key]
		} catch (e) {
			return null;
		}
	},

	// 根据key删除缓存
	remove(key) {
		var key = 'pug_app_' + key;
		try {
			uni.removeStorageSync(key);
			getApp().globalData[key] = null;
		} catch (e) {}
	}

}

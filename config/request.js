import server from './server.js'
import cache from './cache.js'
import utils from './utils.js'

var request = function(app){ 
	// 初始化请求配置
    uni.$u.http.setConfig((config) => {
		config.baseURL = server.baseURL+"/api";
		config.custom.auth = true;
        return config
    })
	
	// 请求拦截 
	uni.$u.http.interceptors.request.use((config) => { 
	    // 初始化请求拦截器时，会执行此方法，此时data为undefined，赋予默认{}
	    config.data = config.data || {auth:true}
		// 获取每个请求中的config的data参数是否要鉴权
		var isAuth = config.custom.auth;
		if(isAuth) {
			// 根据custom参数中配置的是否需要token，添加对应的请求头
			const token = cache.get("token");//从缓存中获取
			const userId = cache.get("userid");//从缓存中获取
			if(token && userId){
				// 给每个需要鉴权的请求地址增加token和userId
				config.header.AuthToken = token;
				config.header.AuthUserId = userId;
			}else{
				// 如果没有token说明没有登录。直接退出到登录
				uni.redirectTo({
					url:"/pages/login/login"
				})
				// //直接走catch，进入到response的catch中去
				return Promise.reject("logout");
			}
		}
		
	    return config 
	}, config => {
	    return Promise.reject(config)
	})
	
	// 响应拦截
	uni.$u.http.interceptors.response.use((response) => { 
		const data = response.data
		// 不等于200说明都是错误返回，进行弹出提示
		if (data.code !== 200) { 
			if(data.code == 609){
				// 如果没有token说明没有登录。直接退出到登录
				uni.redirectTo({
					url:"/pages/login/login"
				});
				return Promise.reject("logout");//直接走catch
			}else{
				if(!utils.isEmpty(data.message)){
					uni.$u.toast(data.message);
				}
				return Promise.reject(data);//直接走catch
			}
		}
		return data === undefined ? {} : data
	}, (responseError) => { 
		if(responseError != 'logout'){
			uni.$u.toast("服务器离开地球表面...");
		}
		return Promise.reject(responseError)
	})
}


module.exports = request;
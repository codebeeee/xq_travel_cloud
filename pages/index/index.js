import bannerService from '@/service/banner/BannerService.js';

export default {
	data() {
		return {
			cityinfo:{
				city:"",
				weather:"",
				temperature:"",
				adcode:""
			},
			list: [
				'7天酒店钟点房打特价啦',
				'7天酒店钟点房打特价啦',
				'7天酒店钟点房打特价啦',
				'7天酒店钟点房打特价啦',
				'奔跑吧暑假'
			],
			swiperlist:[{
				image: 'https://cdn.uviewui.com/uview/swiper/swiper2.png',
				title: '昨夜星辰昨夜风，画楼西畔桂堂东',
			},{
				image: 'https://cdn.uviewui.com/uview/swiper/swiper1.png',
				title: '身无彩凤双飞翼，心有灵犀一点通'
			},{
				image: 'https://cdn.uviewui.com/uview/swiper/swiper3.png',
				title: '谁念西风独自凉，萧萧黄叶闭疏窗，沉思往事立残阳'
			}],
			countrylist:[
				{name:'国内',type:0},
				{name:'国外',type:1},
				{name:'名宿',type:2}
			],
			countrytyle:0,
			show: false,
			mode: 'range',
			startDate:"7月21日",
			endDate:"7月23日",
			nights:1
		}
	},
	
	onLoad() {
		bannerService.loadBanners();
		// 可能会有一次授权
		this.loadCity();
	},
	
	onReady(){
		this.onPlusReady();
	},
	
	watch:{
		['cityinfo.adcode'](newValue, oldValue) {
			if(newValue){
				this.loadWeather();
			}
		}
	},
	
	methods: {
		
		loadWeather(){
			var that = this;
			var code = that.cityinfo.adcode;
			uni.request({
				header:{"Content-Type": "application/text"},
				url:'https://restapi.amap.com/v3/weather/weatherInfo?city='+code+'&key=233e0b3d2a35af7e38d28158c7facbbf&extensions=base',
				success(re) {
					if(re.statusCode===200){
						that.cityinfo.weather= re.data.lives[0].weather
						that.cityinfo.temperature= re.data.lives[0].temperature
					}else{
						console.log("获取信息失败，请重试！")
					}
				 }
			});
		},
		
		onPlusReady(){
			plus.geolocation.getCurrentPosition(function(p){
				console.log('Geolocation\nLatitude:' + p.coords.latitude + '\nLongitude:' + p.coords.longitude + '\nAltitude:' + p.coords.altitude);
			}, function(e){
				console.log('Geolocation error: ' + e.message);
			});
		},
		
		getCity(){
			if(getApp().isEmpty(this.cityinfo.city)){
				this.loadCity();
			}
		},
		
		loadCity(){
			var that = this;
			uni.getLocation({
				type: 'gcj02',
				geocode:true, // 只在android和ios支持
				success: function (res) {
					that.cityinfo.city = res.address?res.address.city:"";
					// #ifndef APP
					uni.request({
						header:{"Content-Type": "application/text"},
						url:'https://restapi.amap.com/v3/geocode/regeo?output=JSON&location='+res.longitude+','+res.latitude+'&key=233e0b3d2a35af7e38d28158c7facbbf&radius=1000&extensions=all',
						success(re) {
							if(re.statusCode===200){
								that.cityinfo.city = re.data.regeocode.addressComponent.city;
								that.cityinfo.adcode = re.data.regeocode.addressComponent.adcode;
								console.log("获取中文街道地理位置成功",re)
							}else{
								console.log("获取信息失败，请重试！")
							}
						 }
					});
					// #endif
				}
			});
		},
		
		
		onTab(type){
			if(type!==this.countrytyle){
				this.countrytyle = type
			}
		},
		searchData(){
			uni.navigateTo({
				url:'/pagessub/hotel/hotel',
			})
		},
		change(e) {
			//获取时间值
			if(e.startMonth==e.endMonth){
				this.nights = e.endDay - e.startDay
			}
			this.startDate = e.startMonth+'月'+e.startDay+'日'
			this.endDate = e.endMonth+'月'+e.endDay+'日'
		},
	}
}
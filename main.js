import App from './App'

// #ifndef VUE3
import Vue from 'vue'
import uView from '@/uni_modules/uview-ui/index.js'

Vue.use(uView)//注册
Vue.config.productionTip = false
App.mpType = 'app'
const app = new Vue({
    ...App
})
//var requets = require('@/config/request.js')
//requets(app);
require('@/config/request.js')(app);
app.$mount()
// #endif

// #ifdef VUE3
import { createSSRApp } from 'vue'
import uView from '@/uni_modules/uview-ui'
export function createApp() {
  const app = createSSRApp(App)
  app.use(uView)
  return {
    app
  }
}
// #endif
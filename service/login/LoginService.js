import server from '@/config/server.js'
export default {
	/**
	 * 短信登录逻辑接口
	 * @param {Object} params
	 */
	toLoginReg(params){
		return uni.$u.http.post(`/passport/login`,params, {custom:{ auth: false}});
	},
	
	/**
	 * 密码登录逻辑接口
	 * @param {Object} params
	 */
	toLoginPwd(params){
		return uni.$u.http.post(`/passport/loginpwd`,params, {custom:{ auth: false}});
	},
	
	
	/**
	 * 退出登录
	 * @param {Object} params
	 */
	toLogout(params){
		return uni.$u.http.post(`/passport/logout`,params, {custom:{ auth: false}});
	},
	
	
	/**
	 * 微信登录获取openid
	 * @param {Object} params
	 */
	weixinLogin(params){
		return uni.$u.http.post(`/passport/wxlogin/code`,params, {custom:{ auth: false}});
	},
	
	/**
	 * 微信登录获取phone
	 * @param {Object} params
	 */
	weixinPhone(params){
		return uni.$u.http.post(`/passport/weixin/phone`,params, {custom:{ auth: false}});
	}
	
}
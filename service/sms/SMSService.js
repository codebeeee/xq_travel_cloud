import server from '@/config/server.js'
export default {
	// 短信发送
	sendPhoneCode(phone){
		return uni.$u.http.post(`/passport/smssend/${phone}`,{},{custom:{ auth: false}});
	}
}
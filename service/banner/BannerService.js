export default {
	
	/**
	 * 查询轮播图列表
	 * @param {Object} params
	 */
	loadBanners(params) {
		return uni.$u.http.post("/banner/load", params);
	}
	
}

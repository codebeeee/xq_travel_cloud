export default {
	/**
	 * 订单消息
	 */
	findOrderMesagePage(params = {
		pageNo: 1,
		pageSize: 10,
		msgtype : ""
	}) {
		return uni.$u.http.post("/message/order/page", params);
	},
	
	/**
	 * 我的消息
	 */
	findUserMesagePage(params = {
		pageNo: 1,
		pageSize: 10,
		msgtype : ""
	}) {
		return uni.$u.http.post("/message/user/page", params);
	},
	
	/**
	 * 系统消息
	 */
	findSysMesagePage(params = {
		pageNo: 1,
		pageSize: 10,
		msgtype : ""
	}) {
		return uni.$u.http.post("/message/sys/page", params);
	},
	
	//保存消息的已读状态
	saveUserMessageStatus(params){
		return uni.$u.http.post("/message/userstatus/save", params);
	}
	
}

export default {
	
	/**
	 * @param {Object} params
	 */
	updateUserInfo(params){
		return uni.$u.http.post("/user/modifty/info",params);	
	},
	
	/**
	 * 同步微信的用户信息到数据库中
	 * @param {Object} params
	 */
	updateUserweixin(params){
		return uni.$u.http.post("/user/update/weixin",params);	
	},
	
	
}
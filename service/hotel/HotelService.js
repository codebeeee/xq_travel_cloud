
export default {
	/**
	 * 搜索和酒店信息
	 * @param {Object} params
	 */
	searchHotels(params){
		return uni.$u.http.post("/hotel/load",params);
	},
	
	/**
	 * 根据id查询酒店明细
	 * @param {Object} hotelId 
	 */
	getHotelDetail(hotelId){
		return uni.$u.http.post(`/hotel/detail/${hotelId}`);
	}
}
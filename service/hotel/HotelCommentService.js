export default {
	/**
	 * 保存酒店评论和回复
	 * @return
	 */
	saveHotelComment(params) {
		return uni.$u.http.post("/hotel/comment/save", params);
	},

	/**
	 * 评论的删除
	 * @return
	 */
	delHotelComment(hotelId,commentId) {
		return uni.$u.http.post(`/hotel/comment/del/${hotelId}/${commentId}`);
	},
	
	
	/**
	 * 评论的查询分页
	 * @return
	 */
	loadHotelComment(params) {
		return uni.$u.http.post(`/hotel/comment/load`,params);
	},
	
	/**
	 * 评论的子查询分页
	 * @return
	 */
	loadHotelChildrenComment(params) {
		return uni.$u.http.post(`/hotel/comment/loadmore`,params);
	},
	
	/**
	 * 评论点赞
	 * @return
	 */
	likeComment(commentId) {
		return uni.$u.http.post(`/hotel/comment/like/${commentId}`);
	},
	
	/**
	 * 评论取消点赞
	 * @return
	 */
	unLikeComment(commentId) {
		return uni.$u.http.post(`/hotel/comment/unlike/${commentId}`);
	}
	
	
	
	
}

var CryptoJS = require("./crypto-js");

//DES 加密 key与后端一样的秘钥(8的倍数)   message(值)
export function encryptByDES(message, key="7396101173961011") {
  var keyHex = CryptoJS.enc.Utf8.parse(key);
  var encrypted = CryptoJS.DES.encrypt(message, keyHex, {
	mode: CryptoJS.mode.ECB,
	padding: CryptoJS.pad.Pkcs7
  });
  return encrypted.toString();
}


//DES 解密
export function decryptByDES(ciphertext, key="7396101173961011") {
  var keyHex = CryptoJS.enc.Utf8.parse(key);
  // direct decrypt ciphertext
  var decrypted = CryptoJS.DES.decrypt({
	ciphertext: CryptoJS.enc.Base64.parse(ciphertext)
  }, keyHex, {
	mode: CryptoJS.mode.ECB,
	padding: CryptoJS.pad.Pkcs7
  });
  return decrypted.toString(CryptoJS.enc.Utf8);
}

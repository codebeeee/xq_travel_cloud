// 导入hotelService
import hotelService from "@/service/hotel/HotelService"
import hotelCommentService from '@/service/hotel/HotelCommentService';

export default {
	data() {
		return {
			toggle:false,
			// 查询当前酒店id
			hotelId:"",
			// 酒店的对象信息
			hotel:{},
			swiperIndex:0,
			typeCurrentIndex:0,
			isExpandDesc:false,
			swiperlist:[],
			hotelTypeItems:[],
			hotelServiceItems:[],
			hotelTypeDetail:{},
			typeSwiperlist:[],
			roomlist:[
				{id:1,image:'https://dimg04.c-ctrip.com/images/02053120008whtqp29D16_R_300_225_R5_Q70_D.jpg',title:'温馨大床房'},
				{id:2,image:'https://dimg04.c-ctrip.com/images/02053120008whtqp29D16_R_300_225_R5_Q70_D.jpg',title:'温馨小床房'},
				{id:3,image:'https://dimg04.c-ctrip.com/images/02053120008whtqp29D16_R_300_225_R5_Q70_D.jpg',title:'高价单人间'}
			],
			show: false,
			show2:false,
			loading:true
		}
	},
	
	onLoad(e){
		// 1： 获取酒店查询的ID
		this.hotelId = e.id;
		// 2： 根据酒店ID查询对应的明细信息
		this.getHotelById();
	},
	
	methods: {
		
		toggleEvent(index){
			var hotelDetail = this.hotelTypeItems[index];
			var {id,hotelId} = hotelDetail;
			this.toggle = true;
			uni.navigateTo({
				url:"/pagesorder/order/order?id="+id
			})
		},
		
		openMap(){
			var lan = this.hotel.lan;
			var lgt =  this.hotel.lgt;
			uni.navigateTo({
				url:"/pagessub/hotelmap/hotelmap?lgt="+lgt+"&lan="+lan
			})
		},
		
		//1：根据酒店ID查询对应的明细信息
		async getHotelById(){
			try{
				var res = await hotelService.getHotelDetail(this.hotelId);
				this.hotel = res.data;
				this.hotelTypeItems = res.data.hotelTypes||[];
				this.hotelServiceItems = res.data.serviceItems||[];
				if(this.hotel && this.hotel.imagesList && this.hotel.imagesList.length > 0){
					this.hotel.imagesList = this.hotel.imagesList.map((img,index)=>{
						var params = {};
						params.id = index;
						params.image = img;
						params.title = index;
						return params;
					});
				}
				setTimeout(()=>(this.loading = false),300);
			}catch(e){
				setTimeout(()=>(this.loading = false),300);
			}
		},
		
		// 2: 改变轮播的索引
		swiperChange(item){
			this.swiperIndex = item.current;
		},
		
		// 3: 控制房型的隐藏和现实
		expandDesc(index){
			this.isExpandDesc = !this.isExpandDesc;
		},
		
		// 4：控制房型明细的弹出层的显示
		expandShow(index){
			this.typeCurrentIndex = 0;
			this.hotelTypeDetail = this.hotelTypeItems[index];
			this.typeSwiperlist = this.hotelTypeDetail.imageLists;
			this.show= true;
		},
	
		
		// 5: 用户点击用户评论打开评论弹窗
		openCommentDialog(){
			// 打开评论弹窗
			this.show2 = true;
			// #ifndef H5			
			// 同时去触发和调起评论的查询
			this.$refs.loadComment.getComment();
			// #endif
		},
				
		addComment(num){
			this.hotel.comments =  this.hotel.comments *1 + num
		},
		
		// 控制房型明细的弹出层的隐藏
		closes(){
			this.show= false
		},
		
		closes2(){
			this.show2= false
		}
		
		
	}
}
// 导入hotelService
import hotelService from "@/service/hotel/HotelService"

export default {
	data() {
		return {
			// 数据容器
			datalist: [],
			// 分页信息
			total: 0,
			pages: 0,
			pageNo: 1,
			pageSize: 10,

			// 搜索信息
			brandId: "",
			categoryId: "",
			distanct: "",
			startPrice: "",
			endPrice: "",
			keyword: "",
			starlevel: "",
			
			// 1:定义一个loading:true
			loading:true,

			// 状态控制
			nextFlag: true,
			loadType: 0,
			themeColor: '#ff5722',
			// 菜单
			menuList: [{
				'title': '品牌',
				'key': 'brandid',
				'isSort': true,
				'detailList': [{
					'title': '全部',
					'value': ''
				},{
					'title': '七天',
					'value': '1'
				}, {
					'title': '华天',
					'value': '2'
				}, {
					'title': '其他',
					'value': '3'
				}]
			}, {
				title: '星级',
				'key': 'startlevel',
				'isMutiple': false,
				'detailTitle': '星级',
				'detailList': [{
					'title': '全部',
					'value': ''
				},{
					'title': '经济型',
					'value': '1'
				}, {
					'title': '舒适/二星',
					'value': '2'
				}, {
					'title': '舒适/三星',
					'value': '3'
				}, {
					'title': '高档/四星',
					'value': '4'
				}, {
					'title': '豪华/五星',
					'value': '5'
				}]
			}, {
				title: '价格',
				'key': 'price',
				'isMutiple': false,
				'detailTitle': '价格',
				'detailList': [
					{
						'title': '不限',
						'value': ['', '']
					},{
					'title': '￥100以下',
					'value': [99, 99]
				}, {
					'title': '￥100-200',
					'value': [100, 200]
				}, {
					'title': '￥200-300',
					'value': [200, 300]
				}, {
					'title': '￥300-400',
					'value': [300, 400]
				}, {
					'title': '￥400-500',
					'value': [400, 500]
				}]
			}, {
				title: '筛选',
				'key': 'category',
				'isMutiple': false,
				'detailTitle': '分类',
				'detailList': [{
					'title': '全部',
					'value': ''
				},{
					'title': '情侣约会',
					'value': '1'
				}, {
					'title': '大床',
					'value': '2'
				}, {
					'title': '双床',
					'value': '3'
				}, {
					'title': '麻将房',
					'value': '4'
				}, {
					'title': '电竞酒店',
					'value': '5'
				}, {
					'title': '民宿',
					'value': '6'
				}, {
					'title': '主题酒店',
					'value': '7'
				}]
			}]
		}
	},

	onLoad() {
		// 1: 使用onLoad方法进行加载酒店数据信息
		this.loadData();
	},

	onShow() {

	},

	// 开发下来刷新实现数据的刷新同步
	onPullDownRefresh() {
		uni.$u.debounce(this.refresh, 500);
	},

	// 专门监听滚动条的滚条到底 <=50时候的触发函数
	onReachBottom() {
		uni.$u.debounce(this.loadMore, 500);
	},

	methods: {

		refresh() {
			// 我要开始刷新了
			this.pageNo = 1;
			this.pageSize = 10;
			this.total = 0;
			this.pages = 0;
			this.datalist = [];
			this.loadData(() => {
				// 执行完毕以后，理解把刷新效果停止掉
				uni.stopPullDownRefresh();
			});
		},


		loadMore() {
			// 防止用户频繁的刷新造成请求过快
			if (this.nextFlag) {
				this.nextFlag = false;
				this.pageNo++;
				if (this.pageNo > this.pages) {
					this.loadType = 2;
					return;
				}
				this.loadData(() => {
					// 查询完毕释放当前锁，让下页继续请求
					this.nextFlag = true;
				});
			}
		},


		// 1: 定义酒店列表查询方法
		loadData(fn) {
			// 1：准备查询和分页参数设置
			var params = {
				pageNo: this.pageNo,
				pageSize: this.pageSize,
				brandId: this.brandId,
				categoryId: this.categoryId,
				distanct: this.distanct,
				startPrice: this.startPrice,
				endPrice: this.endPrice,
				keyword: this.keyword,
				starlevel: this.starlevel,
			};
			// 2: 发起异步请求执行请求查询列表信息
			this.loadType = 0;
			
			hotelService.searchHotels(params).then(res => {
				var {
					pages,
					total,
					records
				} = res.data;
				// 记录数 
				// push(element)
				// 第1页的数组this.datalist = [1,2,3,4,5]
				// 第2页的数组this.datalist = [6,7,8,9,10]
				this.datalist = this.datalist.concat(records);
				// 总共有多少条
				this.total = total;
				// 总共有多少页
				this.pages = pages;
				// 更改状态
				this.loadType = 1;
				
				setTimeout(()=>(this.loading=false),300);
				// 回调函数
				fn && fn();
			})
		},

		result(e) {
			// 搜索前初始化原来的状态，准备开始进行搜索
			this.pageNo = 1;
			this.pageSize = 10;
			this.total = 0;
			this.pages = 0;
			this.datalist = [];
			this.keyword = "";
			this.distanct = "";

			this.brandId = e.brandid || "";
			this.categoryId = e.category || "";
			this.startPrice = e.price[0] || "";
			this.endPrice = e.price[1] || "";
			this.starlevel = e.startlevel || "";
			
			// 执行数据查询
			this.loadData();
		}
	}
}
